# Using bastion.wm.edu

bastion.wm.edu requires authentication either using Duo or SSH keys stored 
on [code.wm.edu](https://code.wm.edu) and does not provide an interactive 
shell. Instead users should use the host as a proxy to jump to other on-campus 
servers from off campus.

You can find detailed instructions for connecting to HPC Clusters on
[a dedicated webpage](https://www.wm.edu/offices/it/services/researchcomputing/using/connecting/index.php).

- [Using bastion.wm.edu](#using-bastionwmedu)
  - [ Getting your keys onto GitLab](#-getting-your-keys-onto-gitlab)
  - [ OpenSSH configuration](#-openssh-configuration)
  - [ PuTTY configuration](#-putty-configuration)

## <a name="gitlab"></a> Getting your keys onto GitLab
To add your SSH keys to code.wm.edu, visit [the SSH Keys settings page](https://code.wm.edu/-/user_settings/ssh_keys) 
in your user settings and paste your *public* key in the key field, give the 
key a suitable title, and click the add key button. Please make sure you paste 
your *public* key and not your private key! Once your public key is available 
on code.wm.edu, the bastion host should find it when you log in, enabling 
passwordless authentication.

If you don't have an SSH key and you use a Unix-like operating system, 
including macOS, follow [Oregon State University's Open Source Labs instructions on generating a key pair](https://wiki.osuosl.org/howtos/ssh_key_tutorial.html).
On up-to-date versions of Windows 10 and Windows 11, consult [Microsoft's 
documentation on OpenSSH key management](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement).

## <a name="openssh"></a> OpenSSH configuration
Invoking the OpenSSH SSH client with the `-J` flag allows you to specify a 
`ProxyJump` host through which to connect to a remote host. To jump through 
bastion.wm.edu, simply put your username@bastion.wm.edu after the `-J` flag 
followed by the remote host you want a shell on, e.g.
```sh
$ ssh -J user@bastion.wm.edu user@somewhere.else
```
You can also specify a `ProxyJump` configuration in your `~/.ssh/config` file
to avoid having to specify the host to jump through at the command line every 
time. To jump through bastion.wm.edu for every wm.edu host you connect to, for 
example, you would add the following to your configuration:
```
Host *.wm.edu !bastion !code.wm.edu
    ProxyJump user@bastion

Host bastion
    HostName bastion.wm.edu
```

## <a name="putty"></a> PuTTY configuration
As of the time of this writing, PuTTY only supports the proxy connections like 
OpenSSH's `ProxyJump` in the pre-release version or in development snapshots, 
which can be downloaded from the [PuTTY homepage](https://www.chiark.greenend.org.uk/%7Esgtatham/putty/). 
The proxy panel allows you to select an SSH proxy to jump through, as 
described in the [pre-release documentation](https://tartarus.org/~simon/putty-prerel-snapshots/htmldoc/Chapter4.html#config-proxy).
